<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="blog padding--both">
    <div class="wrap hpad">
      <?php if (get_field('blog_header', 'options') ) : ?>
        <h2 class="blog__header"><?php echo the_field('blog_header', 'options'); ?></h2>
      <?php endif; ?>

      <?php get_template_part('parts/search', 'bar'); ?>

      <div class="row flex flex--wrap">
        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>

          <?php   
            //post img
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 
          ?>

          <a href="<?php the_permalink(); ?>" class="blog__post col-sm-6 anim fade-up" itemscope itemtype="http://schema.org/BlogPosting">
            
            <div loading="lazy" class="blog__thumb" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);"></div>

            <div class="blog__wrap">
              <header>
                <h2 class="blog__title h3" itemprop="headline" title="<?php the_title_attribute(); ?>">
                    <?php the_title(); ?>
                </h2>
              </header>

              <div class="blog__excerpt" itemprop="articleBody">
                <?php the_excerpt(); ?>
              </div>

              <span class="btn btn--readmore blog__btn"><i></i>Læs mere</span>
            </div>

          </a>

          <?php endwhile; else: ?>

            <p>No posts here.</p>

        <?php endif; ?>
      </div>

      <?php 
        do_action( 'lionlab_pagination' );
      ?>

    </div>
  </section>

  <?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="error padding--both">
  	<div class="wrap hpad">
    	<strong>Beklager, men vi kunne ikke finde siden du søgte efter.</strong>
    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>
<?php get_template_part('parts/header'); ?>

<main itemscope itemtype="http://schema.org/SearchResultsPage">

  <?php get_template_part('parts/page', 'header'); ?>

  <section class="search padding--both"> 

    <div class="wrap hpad">

      <?php if (have_posts()): ?>
        <h2 class="center">
          <strong>Søgeresultat for:</strong>
          <?php echo esc_attr(get_search_query()); ?>
        </h2>
      <?php endif; ?>

      <div class="row flex flex--wrap">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>

            <?php   
              //post img
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 
            ?>

            <a href="<?php the_permalink(); ?>" class="blog__post col-sm-6 anim fade-up" itemscope itemtype="http://schema.org/BlogPosting">
              
              <div class="blog__thumb b-lazy" data-src="<?php echo esc_url($thumb[0]); ?>"></div>

              <div class="blog__wrap">
                <header>
                  <h2 class="blog__title h3" itemprop="headline" title="<?php the_title_attribute(); ?>">
                      <?php the_title(); ?>
                  </h2>
                </header>

                <div class="blog__excerpt" itemprop="articleBody">
                  <?php the_excerpt(); ?>
                </div>

                <span class="btn btn--readmore blog__btn"><i></i>Læs mere</span>
              </div>

            </a>

          <?php endwhile; else: ?>

            <h2>DIn søgning for: <?php echo esc_attr(get_search_query()); ?> gav ingen resultater</h2>

        <?php endif; ?>

      </div>

    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>
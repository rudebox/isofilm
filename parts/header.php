<!doctype html>

<html <?php language_attributes(); ?> class="has-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="4bd9dbce-6cf7-4724-b37d-7019382fce56" data-blockingmode="auto" type="text/javascript"></script>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W8QWKD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<header class="header" itemscope itemtype="http://schema.org/WPHeader">
  <div class="header__container wrap hpad flex flex--center flex--justify">

    <?php 
      //logo SEO markup
      $site_name = get_bloginfo( 'name' );
      $logo_markup  = ( is_front_page() ) ? '<h1 class="visuallyhidden">' . $site_name . '</h1>' : '<p class="visuallyhidden">' . $site_name . '</p>';
    ?>

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>">
      <?php echo $logo_markup; ?>
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

    <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">

      <div class="nav--desktop">
        <?php scratch_main_nav(); ?>
      </div>

      <div class="nav--mobile">
        <div class="wrap hpad">

          <div class="nav__menu--desktop">
            <h2>Sitemap</h2>
            <?php scratch_sitemap_nav(); ?>
          </div>

          <div class="nav__menu--mobile">
            <?php scratch_main_nav(); ?>        
          </div>

        </div>
      </div>
    </nav>

  </div>
</header>

<div id="transition" class="transition">

<div class="transition__container" data-namespace="general"> 

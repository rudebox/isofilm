<?php 
	$img = get_field('sidebar_img');
	$text = get_field('sidebar_text');
	$phone_direct = get_field('sidebar_phone_direct');
	$phone_main = get_field('sidebar_phone_main');
	$mail = get_field('sidebar_mail');
	$title = get_field('sidebar_title');
	$related_links = get_field('sidebar_related');
 ?>

<aside id="sidebar" class="sidebar col-sm-4 col-md-3 col-md-offset-1 bg--grey-dark">
	
	<?php if ($img) : ?>
	<div class="sidebar__img">
		<img loading="lazy" src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
	</div>
	<?php endif; ?>
	<?php if ($text) : ?>
		<h6 class="sidebar__title"><?php echo esc_html($title); ?></h6>
		<p class="sidebar__text"><?php echo esc_html($text); ?></p>
	<?php endif; ?>

	<?php if ($phone_direct) : ?>
		<strong>Direkte: <a class="sidebar__link" href="tel:<?php echo get_formatted_phone($phone_direct); ?>"><?php echo esc_html($phone_direct); ?></a></strong><br>
	<?php endif; ?>

	<?php if ($phone_main) : ?>
		<strong>Hovednr: <a class="sidebar__link" href="tel:<?php echo get_formatted_phone($phone_main); ?>"><?php echo esc_html($phone_main); ?></a></strong><br>
	<?php endif; ?>

	<?php if ($mail) : ?>
		<strong>E-mail: <a class="sidebar__link" href="mailto:<?php echo $mail; ?>"><?php echo esc_html($mail); ?></a></strong>
	<?php endif; ?>
	
	<?php if ($related_links) : ?>
		<div class="sidebar__related">
			<h6 class="sidebar__title">Relateret indhold</h6>
		<?php foreach($related_links as $related_link) : ?>
			<a class="sidebar__link sidebar__link--light btn btn--readmore" href="<?php echo the_permalink($related_link->ID); ?>"><i></i> <?php echo $related_link->post_title; ?></a>
		<?php endforeach; ?>
		</div>
	<?php endif; ?>
	
</aside>
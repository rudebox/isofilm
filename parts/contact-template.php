<?php

/*
 * Template Name: Contact
 */

get_template_part('parts/header'); the_post(); ?>

<?php 
	//contact fields
	$title = get_field('contact_title'); 
	$text = get_field('contact_text'); 

	//social fields
	$fb = get_field('facebook', 'options');
	$ig = get_field('instagram', 'options');
	$lk = get_field('Linkedin', 'options');
	$yt = get_field('youtube', 'options');
?>

<main>

	<?php get_template_part('parts/page', 'header');?>
	
	<section class="contact padding--both">
		<div class="wrap hpad">
			<div class="row flex flex--wrap">

				<div class="col-sm-6 contact__text">					
					<h2 class="contact__title"><?php echo $title; ?></h2>
					<?php echo $text; ?>
					<?php if ($fb) : ?>
					<a class="contact__icon" rel="noopener" target="_blank" class="no-ajax" href="<?php echo esc_url($fb); ?>"><i class="fab fa-facebook-f"></i></a>
					<?php endif; ?>
					<?php if ($fb) : ?>
					<a class="contact__icon" rel="noopener" target="_blank" class="no-ajax" href="<?php echo esc_url($lk); ?>"><i class="fab fa-linkedin-in"></i></a>
					<?php endif; ?> 
					<?php if ($yt) : ?>
					<a class="contact__icon" rel="noopener" target="_blank" class="no-ajax" href="<?php echo esc_url($yt); ?>"><i class="fab fa-youtube"></i></a> 
					<?php endif; ?>
					<?php if ($ig) : ?>
					<a class="contact__icon" rel="noopener" target="_blank" class="no-ajax" href="<?php echo esc_url($ig); ?>"><i class="fab fa-instagram"></i></a>
					<?php endif; ?>
				</div>

				<div class="col-sm-6 contact__form">
					<?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
				</div>
			</div>
		</div>
	</section>

</main>

<?php get_template_part('parts/footer'); ?>


<form class="search__form " method="get" autocomplete="off" action="<?php bloginfo('url'); ?>/">
	<input class="search__input" type="text" value="<?php the_search_query(); ?>" placeholder="<?php _e('Indtast søgeord', 'lionlab'); ?>" name="s" id="s"></input> 
	<button type="submit" class="search__btn"><i class="fas fa-search"></i> </button>
</form>
<?php 
	//video src
	$video_url = get_field('video_yt_link');
	$video_link_url = get_field('video_yt_link_url');

	//poster
	$poster = get_field('video_poster');

	//content
	$text = get_field('video_text');
	$title = get_field('video_title');
	$link = get_field('video_link');
	$link_text = get_field('video_link_text');
?>

<section class="video">
	<div class="video__wrap wrap--fluid flex flex--hvalign" style="background-image: url(<?php echo esc_url($poster['url']); ?>)">
		
		<?php if ($video_url) : ?>
		<div id="ytbg" data-youtube="<?php echo esc_url($video_url); ?>"></div>
		<?php endif; ?>

		<div class="wrap hpad video__container">
			<div class="row flex flex--wrap video__row">

				<div class="video__text col-sm-6">
					<h2 class="h1 video__title"><?php echo $title; ?></h2>
					<?php echo esc_html($text); ?>
					<a class="btn btn--gradient" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
				</div>

				<div class="video__content flex flex--wrap flex--hvalign col-sm-6">
					<a class="video__controls no-ajax" data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo esc_url($video_link_url); ?>?autoplay=1&showinfo=0&controls=0&rel=0">
			    		<div class="video__play"><?php echo file_get_contents('' . get_template_directory_uri() . '/assets/img/play.svg'); ?> </div><span>Afspil video</span>
					</a>
				</div>
			</div>
		</div>

		<div class="video__scroll flex flex--hvalign">
            <a href="#first" class="video__scroll-icon"><i class="fas fa-angle-down"></i></a>
        </div>							

	</div>
</section>
<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	$page_img = get_field('page_img') ? : $page_img = get_field('page_img', 'options');

	$page_video = get_field('page_video');
      
    //post img
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 
          
?>

<?php if (is_single() ) : ?>
<section class="page__hero" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">
<?php else: ?>
<section class="page__hero" style="background-image: url(<?php echo esc_url($page_img['url']) ?>);">
<?php endif; ?>
<?php if ($page_video) : ?>
<div id="ytbg" data-youtube="<?php echo esc_url($page_video); ?>"></div>
<?php endif; ?>
	<div class="wrap hpad page__container">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p class="page__breadcrumbs" id="breadcrumbs">','</p>' );
			}
		?>
		<h1 class="page__title"><?php echo $title; ?></h1>
	</div>
</section>
<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'slider' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'link-boxes' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'link-boxes' ); ?>

    <?php
    } elseif( get_row_layout() === 'about' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'about' ); ?>

    <?php
    } elseif( get_row_layout() === 'references' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'references' ); ?>

    <?php
    } elseif( get_row_layout() === 'employees' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'employees' ); ?>

    <?php
    } elseif( get_row_layout() === 'video-references' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'video-references' ); ?>

    <?php
    } elseif( get_row_layout() === 'price-comparison' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'price-comparison' ); ?>

    <?php
    } elseif( get_row_layout() === 'gallery' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'gallery' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  
<?php
}
?>

<?php 
/**
* Description: Lionlab price comparison repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

//product
$product = get_sub_field('product');

if (have_rows('product_comparison') ) :
?>

<section class="price-tabel padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?> anim fade-up">
	<div class="wrap hpad">

		<?php if ($title) : ?>
			<h3 class="price-tabel__header"><?php echo esc_html($title); ?></h3>
		<?php endif; ?>

		<div class="row flex flex--wrap price-tabel__row">

			<div class="col-sm-3 price-tabel__comparison">

				<span class="price-tabel__spacer"></span>

				<?php 
					while (have_rows('product_comparison') ) : the_row();

					$text = get_sub_field('product_comparison_text');
	 			 ?>

					<p class="price-tabel__text"><?php echo esc_html($text); ?></p>

	 			<?php endwhile; ?>
 			</div>
			
			<?php if (have_rows('product') ) : while (have_rows('product') ) : the_row(); 

				$featured = get_sub_field('product_featured');
				$name = get_sub_field('name');

			?>
				<div class="col-sm-3 price-tabel__product">
					<h4 class="price-tabel__title"><?php echo esc_html($name); ?></h4>

					<?php if (have_rows('product_content') ) : while(have_rows('product_content') ) : the_row(); 
						$content = get_sub_field('product_text');
					?>
						<p class="price-tabel__text"><?php echo esc_html($content); ?></p>
					<?php endwhile; endif; ?>

				</div>
			<?php endwhile; endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>
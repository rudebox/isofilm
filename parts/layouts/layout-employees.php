<?php 
/**
* Description: Lionlab employee repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('employee') ) :
?>

<section class="employees padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="employees__header"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php 
				while (have_rows('employee') ) : the_row();

					$img = get_sub_field('img');
					$name = get_sub_field('name');
					$position = get_sub_field('position');
					$mail = get_sub_field('mail');
					$phone = get_sub_field('phone');
 			 ?>

 			 <div class="col-sm-4 employees__item anim fade-up">
 			 	<?php if(!empty($img) ) : ?>
 			 	<img class="employees__img" loading="lazy" src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
 			 	<?php endif; ?>
 			 	<?php if(empty($img) ) : ?>
 			 	<div class="employees__img--empty"><i class="fas fa-camera"></i></div>
 			 	<?php endif; ?>
 			 	<div class="employees__content">
	 			 	<h5 class="employees__name"><?php echo esc_html($name); ?></h5>
	 			 	<p class="employees__position"><?php echo esc_html($position); ?></p>
	 			 	<?php if ($mail) : ?>
	 			 	E-mail: <a class="employees__link" href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a><br>
	 			 	<?php endif; ?>
	 			 	<?php if ($phone) : ?>
	 			 	Tlf: <a class="employees__link" href="tel:<?php echo esc_html(get_formatted_phone(($phone))); ?>"> <?php echo esc_html($phone); ?></a>
	 			 	<?php endif; ?>
 			 	</div>
 			 </div>

 			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>

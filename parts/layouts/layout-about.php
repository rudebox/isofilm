<?php 
/**
* Description: Lionlab about field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

//ACF fields
$img = get_sub_field('about_img');
$title = get_sub_field('about_title');
$text = get_sub_field('about_text');
$bg_color = get_sub_field('about_bg_color');
$link = get_sub_field('about_link');
$link_text = get_sub_field('about_link_text');
?>

<section class="about bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">

	<div class="about__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);"></div>

	<div class="about__container wrap hpad">
		<div class="row">
			<div class="about__text col-sm-12 bg--<?php echo esc_attr($bg_color); ?>">

				<div class="row">
					<div class="col-sm-10 col-md-6">
						<h2 class="about__title"><?php echo $title; ?></h2>
						<?php echo $text; ?>
						<a class="btn btn--gradient about__btn" href="<?php echo esc_url($link); ?>"><?php echo $link_text; ?></a>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
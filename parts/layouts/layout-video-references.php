<?php 
/**
* Description: Lionlab employee repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('video_references') ) :
?>

<section class="video-references padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="video-references__header"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php 
				while (have_rows('video_references') ) : the_row();

					$link = get_sub_field('video_link');

					//trim url for easier string replacement
					$trim_url = parse_url($link, PHP_URL_PATH);

					//replace and strip string from videolink variable down to video ID for thumbnail use
					$video_id = str_replace('/embed/', '', $trim_url);
 			 ?>

				<div class="col-sm-4 video-references__item anim fade-up">
					<div class="embed embed__item embed--16-9">
					<a class="video-references__link" style="background-image: url(https://img.youtube.com/vi/<?php echo esc_html($video_id); ?>/maxresdefault.jpg);" data-fancybox data-type="iframe" data-src="<?php echo esc_url($link); ?>?autoplay=1&showinfo=0&controls=0&rel=0" href="javascript:;"><div class="video-references__play"><?php echo file_get_contents('wp-content/themes/isofilm/assets/img/play.svg'); ?></div></a>
					</div>		
				</div>
			
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>
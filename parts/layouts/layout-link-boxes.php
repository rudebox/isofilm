<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$text = get_sub_field('intro');

if (have_rows('linkbox') ) :
?>

<section id="first" class="link-boxes bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<h2 class="link-boxes__header"><?php echo $title; ?></h2>
		<div class="row flex flex--wrap">
			<?php if ($text) : ?>
				<div class="link-boxes__intro col-sm-10"><?php echo $text; ?></div>
			<?php endif; ?>
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$img = get_sub_field('icon');
				$link = get_sub_field('link');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-6 link-boxes__item anim fade-up">
				<?php if ($img) : ?>
				<img loading="lazy" src="<?php echo esc_url($img['sizes']['link-boxes']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				<?php endif; ?>
				<div class="link-boxes__wrap">
					<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3>
					<?php echo $text; ?>
					<span class="btn btn--readmore link-boxes__btn"><i></i>Læs mere</span>
				</div>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>
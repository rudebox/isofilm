<?php 
/**
* Description: Lionlab CTA field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$img = get_field('cta_bg', 'options');
$title = get_field('cta_title', 'options');
$text = get_field('cta_text', 'options');
$link = get_field('cta_link', 'options');
$link_text = get_field('cta_link_text', 'options');
?>

<section class="cta padding--both" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
	<div class="wrap hpad">
		<div class="row">
			<div class="cta__item center col-sm-10 col-sm-offset-1">
				<h2 class="cta__title"><?php echo esc_html($title); ?></h2>
				<?php echo $text; ?>
				<a class="btn btn--gradient" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
			</div>
		</div>
	</div>
</section>
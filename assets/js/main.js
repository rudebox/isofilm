jQuery(document).ready(function($) {

  function init() { 
    //in viewport check
    var $animation_elements = $('.anim');
    var $window = $(window);

    function check_if_in_view() {
      var window_height = $window.height();
      var window_top_position = $window.scrollTop();
      var window_bottom_position = (window_top_position + window_height);
     
      $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);
     
        //check to see if this current container is within viewport
        if ((element_top_position <= window_bottom_position)) {
          $element.addClass('in-view');
        } else {
          $element.removeClass('in-view');
        }
      });
    }

    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');

    //menu toggle
    $('.nav-toggle').click(function(e) {
      e.preventDefault();
      $('.nav--mobile').toggleClass('is-open');
      $('body').toggleClass('is-fixed');
      $('.nav--desktop').toggleClass('is-hidden');
    });

    //fancybox
    $('[data-fancybox]').fancybox({ 
      toolbar  : false,
      smallBtn : true,
      iframe : { 
        preload : false 
      }
    });

    //YT video bg
    $(function(){
        $('[data-youtube]').youtube_background(); 
    });

    //responsive YT
    $('iframe[src*="youtube"]').wrap("<div class='embed embed__item embed--16-9'></div>");

    //owl slider/carousel
    var owl = $(".slider__track");

    owl.owlCarousel({
        loop: true,
        items: 1,
        autoplay: true,
        // nav: true,
        autplaySpeed: 11000,
        autoplayTimeout: 10000,
        smartSpeed: 250,
        smartSpeed: 2200,
        navSpeed: 2200
        // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
    });

    // var player; 

// function onYouTubeIframeAPIReady() {
//   player = new YT.Player('video-foreground', {
//     videoId: 'KBJtoMYXxh0', // YouTube Video ID
//     playerVars: {
//       autoplay: 1,        // Auto-play the video on load
//       controls: 0,        // Show pause/play buttons in player
//       showinfo: 0,        // Hide the video title
//       modestbranding: 1,  // Hide the Youtube Logo
//       loop: 1,            // Run the video in a loop
//       fs: 0,              // Hide the full screen button
//       cc_load_policy: 0, // Hide closed captions
//       iv_load_policy: 3,  // Hide the Video Annotations
//       autohide: 0,         // Hide video controls when playing
//       playlist: 'KBJtoMYXxh0'
//     },
//     events: {
//       onReady: function(e) {
//         e.target.mute();
//       }
//     }
//   });
// }


// $(document).ready(function(e) {   
//   $('.sound').on('click', function(){
//     $('#video-foreground').toggleClass('mute');
//     $('.volume-icon').toggleClass('fa-volume-up', 'fa-volume-off');
//     if($('#video-foreground').hasClass('mute')){
//       player.mute();
//     } else {
//       player.unMute();
//     }
//   });
// });

  }

  init();


   //
  // Barba.js
  //
  Barba.Pjax.Dom.wrapperId = 'transition';
  Barba.Pjax.Dom.containerClass = 'transition__container';
  Barba.Pjax.ignoreClassLink = 'no-ajax';

  var general = Barba.BaseView.extend({
    namespace: 'general',
    onEnter: function() {
      // The new Container is ready and attached to the DOM.
    
    },
    onEnterCompleted: function() {
      // The Transition has just finished.
      
    },
    onLeave: function() {
      // A new Transition toward a new page has just started.

    },
    onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.
  
    }
  });


  general.init();

  Barba.Pjax.start();

  Barba.Prefetch.init(); 

  Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;

  Barba.Pjax.preventCheck = function(evt, element) {
    if (!Barba.Pjax.originalPreventCheck(evt, element)) {
      return false;
    }

    //outcomment if you want to prevent barba when logged in
    // if ($('body').hasClass('logged-in')) { Barba.Pjax.preventCheck = function() { 
    //   return false;  
    // }; } 

    // No need to check for element.href -
    // originalPreventCheck does this for us! (and more!)
    if (/.pdf/.test(element.href.toLowerCase())) {
      return false;
    } else if (/edit/.test(element.href.toLowerCase())) {
      return false;
    } else if (/wp-admin/.test(element.href.toLowerCase())) {
      return false;
    }

    return true;
  };

  var FadeTransition = Barba.BaseTransition.extend({
  start: function() {
  /**
  * This function is automatically called as soon the Transition starts
  * this.newContainerLoading is a Promise for the loading of the new container
  * (Barba.js also comes with an handy Promise polyfill!)
  */
  // As soon the loading is finished and the old page is faded out, let's fade the new page
  Promise
  .all([this.newContainerLoading, this.fadeOut()])
  .then(this.fadeIn.bind(this));
  },
  fadeOut: function() {

  /**
  * this.oldContainer is the HTMLElement of the old Container
  */
  //return $(this.oldContainer).animate({ opacity: 0 }).promise();
  return $(this.oldContainer).animate({ opacity: 0 }).promise();
  },
  fadeIn: function() {
    /**
    * this.newContainer is the HTMLElement of the new Container
    * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
    * Please note, newContainer is available just after newContainerLoading is resolved!
    */
    var _this = this;
    var $newContainer = $(this.newContainer);

    $(this.oldContainer).hide();
    $(window).scrollTop(0); // scroll to top here

    $newContainer.css({
      visibility : 'visible',
      opacity: 0
    });

    $newContainer.animate({
      opacity: 1,
    }, 400, function() {
      /**
      * Do not forget to call .done() as soon your transition is finished!
      * .done() will automatically remove from the DOM the old Container
      */
      _this.done();

    });
    }

  });
  /**
  * Next step, you have to tell Barba to use the new Transition
  */
  Barba.Pjax.getTransition = function() {
  /**
  * Here you can use your own logic!
  * For example you can use different Transition based on the current page or link...
  */
  return FadeTransition;

  };

  Barba.Dispatcher.on('initStateChange', function() {

    $('.nav__item').removeAttr('style');
    $('body').removeClass('is-fixed');
    $('.nav--mobile').removeClass('is-open');
    $('.nav__dropdown').removeClass('is-visible');
    $('.nav--desktop').removeClass('is-hidden');
    
    init();

    //send page view on every page transition
    if (Barba.HistoryManager.history.length <= 1) {
        return;
    }

    if (typeof gtag === 'function') {
        // send statics by Google Analytics(gtag.js)
        gtag('config', 'GTM-W8QWKD', {'page_path': location.pathname, 'use_amp_client_id': true});
    } 

    else {
        // send statics by Google Analytics(analytics.js) or Google Tag Manager
        if (typeof ga === 'function') {
            var trackers = ga.getAll();
            trackers.forEach(function (tracker) {
                ga(tracker.get('name') + '.send', 'pageview', location.pathname, {'useAmpClientId': true});
            });
        }
    }

  });

  /**
  * GET WP body classes
  */
  Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container, newPageRawHTML) {

    // html head parser borrowed from jquery pjax
    var $newPageHead = $( '<head />' ).html(
        $.parseHTML(
            newPageRawHTML.match(/<head[^>]*>([\s\S.]*)<\/head>/i)[0]
          , document
          , true
        )
    );

    var headTags = [
        "meta[name='keywords']"
      , "meta[name='description']"
      , "meta[property^='og']"
      , "meta[name^='twitter']"
      , "meta[itemprop]"
      , "link[itemprop]"
      , "link[rel='prev']"
      , "link[rel='next']"
      , "link[rel='canonical']"
    ].join(',');
    $( 'head' ).find( headTags ).remove(); // Remove current head tags
    $newPageHead.find( headTags ).appendTo( 'head' ); // Append new tags to the head 


    //update body classes
    var response = newPageRawHTML.replace(/(<\/?)body( .+?)?>/gi, '$1notbody$2>', newPageRawHTML)
    var bodyClasses = $(response).filter('notbody').attr('class')
    $('body').attr('class', bodyClasses);


    //get current url and path
    var $path = currentStatus.url.split(window.location.origin)[1].substring(1); 
    var $url = currentStatus.url;

    //add active class based upon URL status
    $('.nav__item a').each(function() {  
      if ($url === this.href) {
        $(this).addClass('is-active');
      }

      else {
        $(this).removeClass('is-active');
        $('.nav__item').removeClass('is-active');
      }
    }); 

    init();

  }); 

  Barba.Dispatcher.on('transitionCompleted', function (currentStatus, oldStatus, container) {

  });

});

<?php

/*
 * Template Name: Layouts
 */

get_template_part('parts/header'); the_post();

$sidebar_check = get_field('sidebar');

?>

<main>
	
	<?php get_template_part('parts/page', 'header');?>

	<?php if ($sidebar_check === true) : ?>
		<section class="page__content padding--both">
			<div class="wrap hpad">
				<div class="row">
					<div class="col-sm-7 col-md-8 page__col">
						<?php the_content(); ?>
					</div>
					<?php get_template_part('parts/sidebar'); ?>
				</div>
			</div>
		</section>
	<?php endif; ?>

	<?php get_template_part('parts/content', 'layouts'); ?>
	<?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>

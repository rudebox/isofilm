<?php

/* Do not remove this line. */
require_once('includes/scratch.php');
require_once('lib/theme-dependencies.php');


/*
 * adds all meta information to the <head> element for us.
 */

function scratch_meta() { ?>

  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?php bloginfo('description'); ?>">

  <link rel="apple-touch-icon" href="/apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-W8QWKD');</script>

  <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','//connect.facebook.net/en_US/fbevents.js');
      
      fbq('init', '1612193359029404');
      fbq('track', 'PageView');
  </script>

  <noscript>
    <img height="1" width="1" alt="FBhidden" style="display:none" src="https://www.facebook.com/tr?id=1612193359029404&ev=PageView&noscript=1" />
  </noscript>

  <meta name="google-site-verification" content="QQSEOi8rSU-GvYCNUoSxVw3x9m5nJD-pEY8K2TIw824"/>

<?php }

add_action('wp_head', 'scratch_meta');


/**
 * Add fonts to header for preload option insted of importing through css in variables
 */
function lionlab_load_fonts() { ?>
  <link rel="preload" as="font" crossorigin="anonymous" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/HelveticaNeueLight.ttf" type="font/woff2"> 
  <link rel="preload" as="font" crossorigin="anonymous" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/HelveticaNeueBold.ttf" type="font/woff2">  
<?php }

add_action('wp_head', 'lionlab_load_fonts');

/* Theme CSS */

function theme_styles() {

  wp_enqueue_style( 'normalize', 'https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.min.css', false, null );

  wp_enqueue_style( 'fontawesome', 'https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.14.0/css/all.min.css', false, null );

  wp_enqueue_style( 'fancybox', 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css', true, null );

  wp_register_style( 'scratch-main', get_template_directory_uri() . '/assets/css/master.css', false, filemtime(dirname(__FILE__) . '/assets/css/master.css') );
  wp_enqueue_style( 'scratch-main' );

}

add_action( 'wp_enqueue_scripts', 'theme_styles' );

/* Theme JavaScript */

function theme_js() {

  if (!is_admin()) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js', array(), null, true );
  }

  wp_register_script( 'scratch-main-concat', get_template_directory_uri() . '/assets/js/concat/main.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/concat/main.js'), true ); 

  wp_register_script( 'scratch-main-min', get_template_directory_uri() . '/assets/js/build/main.min.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/build/main.min.js'), true );

  wp_enqueue_script( 'barba', 'https://cdn.jsdelivr.net/npm/barba.js@1.0.0/dist/barba.min.js', array(), null, true );

  wp_enqueue_script( 'fancybox', 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js', array(), null, true ); 

  /* FOR DEVELOPMENT */
  // wp_enqueue_script( 'scratch-main-concat' );

  /* FOR PRODUCTION */
  wp_enqueue_script( 'scratch-main-min' );

}

add_action( 'wp_enqueue_scripts', 'theme_js' );


//defer JS
function defer_parsing_of_js( $url ) {
    if ( is_user_logged_in() ) return $url; //don't break WP Admin
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    if ( strpos( $url, 'jquery.min.js' ) ) return $url;
    return str_replace( ' src', ' defer src', $url );
}
add_filter( 'script_loader_tag', 'defer_parsing_of_js', 10 );


/* Enable ACF Options Pages */


if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Globalt indhold',
    'menu_title'  => 'Globalt indhold',
    'menu_slug'   => 'global-content',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Header',
    'menu_title'  => 'Header',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Sidebar',
    'menu_title'  => 'Sidebar',
    'parent_slug' => 'global-content',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Footer',
    'menu_title'  => 'Footer',
    'parent_slug' => 'global-content',
  ));
}

/* Enable Featured Image */

add_theme_support( 'post-thumbnails' );

// Image sizes
add_image_size('link-boxes', 800, 533, true);
add_image_size('employees', 436, 245, true);
add_image_size('gallery', 478, 268, true);

// Add created images sizes to dropdown in WP control panel
add_filter( 'image_size_names_choose', 'custom_image_sizes' );

function custom_image_sizes( $sizes ) {
  return array_merge( $sizes, array( 
    
  ) );
} 

/* Enable Custom Menus */

add_theme_support( 'menus' );

register_nav_menus(
  array(
    'scratch-main-nav' => __( 'Main Nav', 'scratch' ),   // main nav in header'
    'scratch-sitemap-nav' => __( 'Sitemap Nav', 'scratch' )   // sitemap nav in header
  )
);

function scratch_main_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Main Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-main-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end scratch main nav */


function scratch_sitemap_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Sitemap Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu nav__menu--sitemap', // adding custom nav class
    'theme_location' => 'scratch-sitemap-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end scratch main nav */

/* Place custom functions below here. */

/* Don't delete this closing tag. */
?>
<?php 
  get_template_part('parts/header'); the_post(); 
  $sidebar_check = get_field('sidebar');
?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="single padding--both">
    <div class="wrap hpad">

      <?php 
        $author = get_the_author(); 
        $linkedin = get_the_author_meta( 'linkedin', $post->post_author );

        $author_id = get_the_author_meta('ID');
        $avatar = get_field('avatar', 'user_'. $author_id );
      ?>
      
      <?php if ($sidebar_check === true) : ?>
        <div class="row">
          <article class="single__post col-sm-8" itemscope itemtype="http://schema.org/BlogPosting">

            <div class="single__meta flex flex--justify flex--center flex--wrap">
            
              <div class="single__wrap flex flex--center">
                <?php if ($avatar) : ?>
                <img loading="lazy" class="single__avatar" src="<?php echo $avatar['url']; ?>" alt="<?php echo esc_html($author); ?>">
                <?php endif; ?>
                <div class="single__inner-wrap">
                  <strong class="single__name"><?php echo esc_html($author); ?></strong><br>
                  <?php if (get_the_author_meta('description') ) : ?>
                  <em class="single__position red"><?php echo (get_the_author_meta('description') ); ?></em>
                  <?php endif; ?>
                </div>
              </div>

              <a target="_blank" class="single__icon" href="<?php echo esc_url(get_the_author_meta( 'linkedin', $post->post_author ) ); ?>"><i class="fab fa-linkedin-in"></i></a>
            </div>

            <div itemprop="articleBody">
              <?php the_content(); ?>
            </div>

          </article>

          <?php get_template_part('parts/sidebar'); ?>

        </div>
      <?php else: ?>
         <article class="single__post" itemscope itemtype="http://schema.org/BlogPosting">

            <div class="single__meta flex flex--justify flex--center flex--wrap">
            
              <div class="single__wrap flex flex--center">
                <?php if ($avatar) : ?>
                <img class="single__avatar" src="<?php echo $avatar['url']; ?>" alt="<?php echo esc_html($author); ?>">
                <?php endif; ?>
                <div class="single__inner-wrap">
                  <strong class="single__name"><?php echo esc_html($author); ?></strong><br>
                  <?php if (get_the_author_meta('description') ) : ?>
                  <em class="single__position red"><?php echo (get_the_author_meta('description') ); ?></em>
                  <?php endif; ?>
                </div>
              </div>

              <a rel="noopener" target="_blank" class="single__icon" href="<?php echo esc_url(get_the_author_meta( 'linkedin', $post->post_author ) ); ?>"><i class="fab fa-linkedin-in"></i></a>
            </div>

            <div itemprop="articleBody">
              <?php the_content(); ?>
            </div>

          </article>
      <?php endif; ?>
    </div>
  </section>
  
  <?php 
    $title = get_field('newsletter_title', 'options');
    $text = get_field('newsletter_text', 'options');
    $id = get_field('newsletter_id', 'options');
    $bg = get_field('newsletter_bg', 'options');
  ?>
  
  <section class="news-letter padding--both b-lazy" data-src="<?php echo esc_url($bg['url']); ?>">
    <div class="wrap hpad center">
      <h2 class="news-letter__title"><?php echo esc_html($title); ?></h2>
      <?php echo $text; ?>
      <?php echo do_shortcode('[mc4wp_form id="' . $id .'"]'); ?>
    </div>
  </section>


</main>

<?php get_template_part('parts/footer'); ?>